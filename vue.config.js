module.exports = {
  pluginOptions: {
    autoRouting: {
      chunkNamePrefix: "page-"
    },
    env: {
      APP_NAME: "Base.JS",
      API_URL: "http://localhost:3000/api"
    }
  },
  transpileDependencies: ["vuetify"]
};
