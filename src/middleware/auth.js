export default function auth({ next, store }) {
  if (!localStorage.getItem('token')) {
    return next({
      name: "login"
    });
  }

  return next();
}
