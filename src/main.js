import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import { store } from "./store/index";
import VueProgressBar from "vue-progressbar";
import axios from "axios";

import vuetify from './plugins/vuetify';
require("@/assets/css/default.scss");
require("@/assets/css/font-awesome/css/all.css");

Vue.use(VueProgressBar, {
  color: "rgba(3, 168, 124, 1)",
  failedColor: "red",
  height: "2px"
});

axios.defaults.baseURL = process.env.API_URL;
axios.defaults.headers.common["Authorization"] = localStorage.getItem("token");

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
