const state = {
    token: null,
    user: {},
    loggedIn: false,
}

const getters = {
    token: (state) => state.token,
    user: (state) => state.user,
    loggedIn: (state) => state.loggedIn,
}

const mutations = {
    SET_AUTH: (state) => {
        state.token = localStorage.getItem('token'),
            state.user = JSON.parse(localStorage.getItem('user')),
            state.loggedIn = Boolean(localStorage.getItem('token'))
    },
    CLEAR_AUTH: (state) => {
        state.token = null
        state.user = null
        state.loggedIn = false
    },
}

const actions = {
    setAuth: ({ commit }) => {
        commit('SET_AUTH')
    },
    clearAuth: ({ commit }) => {
        commit('CLEAR_AUTH')
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}